const express = require('express');
const path = require('path');
const history = require('connect-history-api-fallback');
const cron = require('node-cron');
const app = express();
const axios = require('axios')

var env = require('node-env-file'); // .env file
env(__dirname + '/.env');

//start cron job
  cron.schedule('* * * * *', function () {
    const data = {
      'cron_token': process.env.CRON_TOKEN
    }
    axios.post(process.env.CUOTAS_API_URL, data)
    .then(function (response) {
     console.log(response);
   })
   .catch(function (error) {
     console.log(error);
   })
   .finally(function () {
     console.log('Running Cron Job');
   });
 });
//end cron job

const staticFileMiddleware = express.static(path.join(__dirname + '/dist'));

app.use(staticFileMiddleware);
app.use(history({
  disableDotRule: true,
  verbose: true
}));
app.use(staticFileMiddleware);

app.get('/', function (req, res) {
  res.render(path.join(__dirname + '/dist/index.html'));
});

var server = app.listen(process.env.PORT || 8080, function () {
  var port = server.address().port;
  console.log("App now running on port", port);
});
